# PUA framework demo

This repo contains:

An in-depth installation and use guide

A demo that uses the PUA framework that developers can use as a guide for their development

It also contains “best practices” suggested for developers.


# Overview

There is no “absolute security”.  The term security means that under some given assumptions about the system, no attack of a certain form will compromise the system’s specifications. 

This document explains the PUA (Persistent User Authentication) SDK (Framework–API), developed by The Whisper Company for a more secure & private HMI (Human-Machine Interface).  Additional information is available in the GitLab repositories (https://gitlab.com/the_whisper_company/twc.pua for iOS and https://gitlab.com/the_whisper_company/twc.pua_android for Android). 

Persistent User Authentication (PUA)

The PUA (Persistent User Authentication), as the name indicates, is an SDK/Framework/Library/API that enables app developers to embed in their apps an end-point (mobile or desktop platform’s interface) continuous user authorization tool.  It is the “last yard” on a private and secure high-trust environment solution.

High-Trust (More Secure UI)
Instantaneous response when authorized user is not in from of the device (disengages) or when a second face is in the camera field of view (second-face detection).  
Suggested to “Pause” (Halt) the application and “Obfuscate” the screen (ability to add your logo for branding your security)
Reduce the risks of unauthorized access and use of your app by an unauthorized user impersonating your client.  For example if the device is left unattended before the automatic time-out occurs or the user was forced to unlock the device.
Guarantee (almost as a notary) that only the authorized user can operate your app.

Seamless (Less Complex UX)
If you “Pause” the app, as soon as the authorized user re-engages the device–returns to the field of view of the camera, your app can “Resume” instantly, without the need for multi-factor authentication or the user needing to login. Again.
It makes the use of your app seamless to the user.  Animation is suggested to let your customer that (s)he has been authenticated and can resume operating the app.  This is another opportunity to reinforce the security of your brand.

PUA reimagines the HMI for high-trust environments.  It sits between your app or device and the user, by continuously using biometrics to authenticate that the authorized user is the current user of the app or device.  After an initial authentication and authorization using biometrics (or any other method implemented by the developer), it uses the device camera to track the (just) authenticated user.  So, it goes beyond authentication to grant access, it guarantees that the authenticating user is operating the app/device.  As long as the user stays in the camera field of view (CFOV), no other face is in the CFOV, and the gaze of the user is focused on the device (user’s attention), PUA lets the app know that the authorized user is operating the device/app.

## Getting Started

After downloading this repository, please make sure you are running Xcode 13+, Swift 5.5+ and your iOS device has installed iOS 15+

## Compatibility

This project is written in Swift 5.6 and requires Xcode 13.3 or newer to build and run.
PUA is compatible with iOS 15.4+.


## Installation

First, create an Xcode project if you have not done so.  Then, paste your `PUA.framework` file into the root folder of your project:

The first thing the developer must do is download the repository using any of the tools (s)he is familiar with.  It could be Git, GitHub, GitLab, SourceTree, XCode, etc.  The repository is available at:

https://gitlab.com/the_whisper_company/twc.pua for iOS ,and 

https://gitlab.com/the_whisper_company/twc.pua_android for Android

After the download, the developer will need to make sure that the selected development environment is available in the developer’s workstation.  This would be Xcode for iOS targets or Android Studio for Android deployment.

<br>
<p align="center">
  <img src="./resources/folder-structure.png"/>
</p>
<br>
Then go to your project's main target and scroll down to _Frameworks, Libraries, and Embedded Content_:
<br>
<p align="center">
  <img src="./resources/frameworks-libraries.png"/>
</p>
<br>
Then click the _add_ button and select _add files_:
<br>
<p align="center">
  <img src="./resources/add-framework.png"/>
</p>
<br>
Go to your project's root folder and select your `PUA.framework` file and click open:
<br>
<p align="center">
  <img src="./resources/select-PUA-framework.png"/>
</p>
<br>
Then, go to your _`info.plist`_ and add the following keys:
<p align="center">
  <img src="./resources/info-keys.png"/>
</p>
Make sure to give an accurate description of your usage.
<br>

And you are ready to use the framework in your project. Keep in mind that `PUA.framework` only works for physical devices. You might get a build error if your target platform is not a physical device. If you want to build without a physical device, you can build for the _`Any iOS device`_ platform.

## Activating your license

Go to your `AppDelegate` and call the following in your `application(_ application:, didFinishLaunchingWithOptions launchOptions:)`:

```swift
let PUAAPIKey = "YOUR-API-KEY"
WCPUA.authenticate(APIKey: PUAAPIKey)
```

You do not have to call this specifically in `application(_ application:, didFinishLaunchingWithOptions launchOptions:)`, be sure to call it once per application execution and before using the PUA. If you try to use the PUA without authenticating, the PUA services won't start.

## Using the PUA

First, import the `PUA.framework`.

```swift
import PUA
```

The PUA uses the protocol design pattern to delegate the actions of each authentication case.

Two protocols must be confirmed, the `WCPUALoggerProtocol` and the `WCPUADelegate` protocol.

The `WCPUALoggerProtocol` is used the each time the PUA wants to log information. A simple implementation of `WCPUALoggerProtocol` could be:

```swift
// MARK: - WCPUALoggerProtocol
extension ViewController: WCPUALoggerProtocol {
    func error(_ object: Any) {
        print(object)
    }

    func info(_ object: Any) {
        print(object)
    }

    func debug(_ object: Any) {
        print(object)
    }

    func verb(_ object: Any) {
        print(object)
    }

    func warn(_ object: Any) {
        print(object)
    }

    func severe(_ object: Any) {
        print(object)
    }
}
```

The `WCPUADelegate` is used to report events relevant to the PUA, such as authentication success or fails. A simple implementation of `WCPUADelegate` could be:

```swift
// MARK: - WCPUADelegate
extension ViewController: WCPUADelegate {
    func onConfigFailed() {
        print("PUA Config failed")
    }

    func onAuthFailed() {
        print("PUA auth failed")
    }

    func onAuthSuccess() {
        print("PUA auth success")
    }

    func onGenericError() {
        print("PUA generic error")
    }

    func onLightWarning() {
        print("Is too dark, PUA might not work correctly")
    }
}
```
    class WCPUATwoConfig {
        public var faceOffScreenAllowedTime = 0.25 // seconds
        public var eyesOffScreenAllowedTime = 10.0 // seconds
        public let eyesPositionMarginOfErrorPoints = Float(0.5)
        public var numberOfFacesAllowed = 1
        public let numberOfFacesRecognitionRate = 0.5
        public var authenticationMinimumInterval = 2.0 // seconds
        public let validNumberOfFaces = ["1", "2", "3", "4"]
        
        public let accelerationWarningThreshold = 1.0
        public var lowLightThreshold = 400.0

Once you have confirmed `WCPUALoggerProtocol` and `WCPUADelegate`, you can configure and start the PUA:

```swift
import UIKit
import PUA

// MARK: - Definition
class ViewController: UIViewController {

    private var PUA: WCPUA?

    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.PUA = WCPUA()
            self.PUA?.logger = self
            self.PUA?.delegate = self
            self.PUA?.config()
            DispatchQueue.global(qos: .userInitiated).async {
                self.PUA?.start()
            }
        }
    }
}
```

Once started, the PUA will try to authenticate the face of the user. There are two possibles outcomes: authentication fails or success. If the authentication fails, the delegate's `onAuthFailed` method gets called. Otherwise, the delegate's `onAuthSuccess` method gets called.

The delegate's `onConfigFailed` will be called if any unexpected error occurs while configuring the PUA.

The delegate's `onGenericError` will be called if any unexpected error occurs after starting the PUA.

The delegate's `onLightWarning` will be called if the estimated ambient light is very low.

In general, the event flow of the PUA is the following:

<br>
<p align="center">
  <img src="./resources/PUA-diagram.jpg"/>
</p>
<br>

You can react to the PUA events as you want. For example, in this Demo, we use `onAuthFailed` to hide the UI and `onAuthSuccess` to show the UI again.

```swift
// MARK: - PUA delegate
extension DemoViewController: WCPUADelegate {
    func onConfigFailed() {
        PUAStatusLabel.text = "Demo not available"
        PUAMessageLabel.text = "Failed to configure PUA"
        backgroundView.backgroundColor = UIColor.systemRed
    }

    func onAuthFailed() {
        if WCPUA.getVersion() == "2.0" { return }

        if self.state == "visible" {
            self.animateHideShowSecureText()
        }

        self.state = "hidden"
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "Authentication failed"
        backgroundView.backgroundColor = UIColor.systemRed
    }

    func onAuthSuccess() {
        if self.state == "hidden" {
            self.animateShowSecureText()
        }
        self.state = "visible"
        PUAStatusLabel.text = "PUA Unlocked"
        PUAMessageLabel.text = "Biometric authentication successful"
        backgroundView.backgroundColor = UIColor.systemBlue
    }

    func onGenericError() {
        PUAStatusLabel.text = "Demo not available"
        PUAMessageLabel.text = "PUA fatal generic error"
        backgroundView.backgroundColor = UIColor.systemRed
    }

    func onLightWarning() {
        guard !self.PUAlowLightWarned else {
            return
        }

        self.PUAlowLightWarned = true
        let alert = UIAlertController(title: "Low lightning",
                                      message: "PUA might not work properly.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
`

## Demo

This PUA Demo demonstrates all functionality of the PUA framework. The PUA framework can be used to protect sensitive information that only the intended user can see. This framework is a continuation of the [invisible-ink [i-i] app](https://apps.apple.com/us/app/i-i/id1508031588). If you would like an API key to test this Demo, please contact The Whisper Company at [info@thewhispercompany.com](mailto:info@thewhispercompany.com) or [juan.rincon@thewhispercompany.com](mailto:juan.rincon@thewhispercompany.com)

The Demo authenticates the user via FaceID and constantly checks if it is the same user.

<p align="center">
  <img src="./resources/PUA-success.PNG", width="400"/>
</p>

If it detects another face, it will alert the app.

<p align="center">
  <img src="./resources/two-faces.PNG", width="400"/>
</p>

If it detects the user is not looking at the device, it will alert the app. You may customize the trigger time.

<p align="center">
  <img src="./resources/not-looking.PNG", width="400"/>
</p>

Lastly, if it does not detect any user, it will alert the app.

<p align="center">
  <img src="./resources/PUA-lock.PNG", width="400"/>
</p>

You may adjust all settings, including the refresh rate, on the settings page.

<p align="center">
  <img src="./resources/Settings.jpeg", width="400"/>
</p>

## Instalation

First, create an Xcode project if you have not done so. Then, paste your `PUA.framework` file into the root folder of your project:

<br>

<p align="center">
  <img src="./resources/folder-structure.png"/>
</p>

<br>

Then go to your project's main target and scroll down to _Frameworks, Libraries, and Embedded Content_:

<br>

<p align="center">
  <img src="./resources/frameworks-libraries.png"/>
</p>

<br>

Then click the _add_ button and select _add files_:

<br>

<p align="center">
  <img src="./resources/add-framework.png"/>
</p>

<br>

Go to your project's root folder and select your `PUA.framework` file and click open:

<br>

<p align="center">
  <img src="./resources/select-PUA-framework.png"/>
</p>

<br>

Then, go to your _`info.plist`_ and add the following keys:

<p align="center">
  <img src="./resources/info-keys.png"/>
</p>

Make sure to give an accurate description of your usage.

<br>

And you are ready to use the framework in your project. Keep in mind that `PUA.framework` only works for physical devices. You might get a build error if your target platform is not a physical device. If you want to build without a physical device, you can build for the _`Any iOS device`_ platform.

## Activating your license

Go to your `AppDelegate` and call the following in your `application(_ application:, didFinishLaunchingWithOptions launchOptions:)`:

```swift
let PUAAPIKey = "YOUR-API-KEY"
WCPUA.authenticate(APIKey: PUAAPIKey)
```

You do not have to call this specifically in `application(_ application:, didFinishLaunchingWithOptions launchOptions:)`, be sure to call it once per application execution and before using the PUA. If you try to use the PUA without authenticating, the PUA services won't start.

## Using the PUA

First, import the `PUA.framework`.

```swift
import PUA
```

The PUA uses the protocol design pattern to delegate the actions of each authentication case.

Two protocols must be confirmed, the `WCPUALoggerProtocol` and the `WCPUADelegate` protocol.

The `WCPUALoggerProtocol` is used the each time the PUA wants to log information. A simple implementation of `WCPUALoggerProtocol` could be:

```swift
// MARK: - WCPUALoggerProtocol
extension ViewController: WCPUALoggerProtocol {
    func error(_ object: Any) {
        print(object)
    }

    func info(_ object: Any) {
        print(object)
    }

    func debug(_ object: Any) {
        print(object)
    }

    func verb(_ object: Any) {
        print(object)
    }

    func warn(_ object: Any) {
        print(object)
    }

    func severe(_ object: Any) {
        print(object)
    }
}
```

The `WCPUADelegate` is used to report events relevant to the PUA, such as authentication success or fails. A simple implementation of `WCPUADelegate` could be:

```swift
// MARK: - WCPUADelegate
extension ViewController: WCPUADelegate {
    func onConfigFailed() {
        print("PUA Config failed")
    }

    func onAuthFailed() {
        print("PUA auth failed")
    }

    func onAuthSuccess() {
        print("PUA auth success")
    }

    func onGenericError() {
        print("PUA generic error")
    }

    func onLightWarning() {
        print("Is too dark, PUA might not work correctly")
    }
}

```

Once you have confirmed `WCPUALoggerProtocol` and `WCPUADelegate`, you can configure and start the PUA:

```swift
import UIKit
import PUA

// MARK: - Definition
class ViewController: UIViewController {

    private var PUA: WCPUA?

    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.PUA = WCPUA()
            self.PUA?.logger = self
            self.PUA?.delegate = self
            self.PUA?.config()
            DispatchQueue.global(qos: .userInitiated).async {
                self.PUA?.start()
            }
        }
    }
}
```

Once started, the PUA will try to authenticate the face of the user. There are two possibles outcomes: authentication fails or success. If the authentication fails, the delegate's `onAuthFailed` method gets called. Otherwise, the delegate's `onAuthSuccess` method gets called.

The delegate's `onConfigFailed` will be called if any unexpected error occurs while configuring the PUA.

The delegate's `onGenericError` will be called if any unexpected error occurs after starting the PUA.

The delegate's `onLightWarning` will be called if the estimated ambient light is very low.

In general, the event flow of the PUA is the following:

<br>

<p align="center">
  <img src="./resources/PUA-diagram.jpg"/>
</p>

<br>

You can react to the PUA events as you want. For example, in this Demo, we use `onAuthFailed` to hide the UI and `onAuthSuccess` to show the UI again.

```swift
// MARK: - PUA delegate
extension DemoViewController: WCPUADelegate {
    func onConfigFailed() {
        PUAStatusLabel.text = "Demo not available"
        PUAMessageLabel.text = "Failed to configurate PUA"
        backgroundView.backgroundColor = UIColor.systemRed
    }

    func onAuthFailed() {
        if WCPUA.getVersion() == "2.0" { return }

        if self.state == "visible" {
            self.animateHideShowSecureText()
        }

        self.state = "hidden"
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "Authentication failed"
        backgroundView.backgroundColor = UIColor.systemRed
    }

    func onAuthSuccess() {
        if self.state == "hidden" {
            self.animateShowSecureText()
        }
        self.state = "visible"
        PUAStatusLabel.text = "PUA Unlocked"
        PUAMessageLabel.text = "Biometric authentication sucessfull"
        backgroundView.backgroundColor = UIColor.systemBlue
    }

    func onGenericError() {
        PUAStatusLabel.text = "Demo not available"
        PUAMessageLabel.text = "PUA fatal generic error"
        backgroundView.backgroundColor = UIColor.systemRed
    }

    func onLightWarning() {
        guard !self.PUAlowLightWarned else {
            return
        }

        self.PUAlowLightWarned = true
        let alert = UIAlertController(title: "Low lightning",
                                      message: "PUA might not work properly.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
```

## Advanced - PUA versions

PUA has two parallel versions, _`WCPUAOne`_ and _`WCPUATwo`_. Both versions share the same API via the _`WCPUA`_ facade. _`WCPUATwo`_ is the most recent and reliable version. It supports eye-tracking, accurate face tracking, better lighting conditions management, and better multiple face detection. However, _`WCPUATwo`_ is only available on devices that support _`ARFaceTracking`_. The _`WCPUA`_ facade automatically uses the best version available on the running device.

You can check which version of PUA is running with:

```swift
WCPUA.getVersion()
```

## Advanced - PUA parameters

You can feed each instance of _`WCPUA`_ with parameters. Each version of _`WCPUA`_ has its own set of parameters.

_`WCPUAConfig`_ is the class that allows you to customize parameters. It receives the parameters for _`WCPUAOne`_ and _`WCPUATwo`_ in the form of _`WCPUAConfig.WCPUAOneConfig`_ and _`WCPUAConfig.WCPUATwoConfig`_:

```swift
DispatchQueue.main.async {
    let PUAOneParameters = WCPUAConfig.WCPUAOneConfig()
    // ... Edit PUAOne parameters

    let PUATwoParameters = WCPUAConfig.WCPUATwoConfig()
    // .. Edit PUATwo parameters

    self.PUA = WCPUA(parameters: WCPUAConfig(PUAOneConfig: PUAOneParameters, PUATwoConfig: PUATwoParameters))
    self.PUA?.logger = self
    self.PUA?.delegate = self
    self.PUA?.config()
    DispatchQueue.global(qos: .userInitiated).async {
        self.PUA?.start()
    }
}
```

Here is the list of parameters for _`WCPUAConfig.WCPUAOneConfig`_:

- _`interval`_: frequency (in seconds) of tracking. default is `3` seconds.
- _`lowLightThreshold`_: Minimum allowed of estimated ambient luminosity. If the estimators return a _Lux value_ minor than the threshold, the delegate's `onLightWarning` will be called. Tweak the value of this parameter so that the alert gets called whenever your app expects it. Default is `900`.

Here is the list of parameters for _`WCPUAConfig.WCPUATwoConfig`_:

- _`faceOffScreenAllowedTime`_: Maximum time (in seconds) of _face tracking lost_ before going back to _looking for face_. Default is `0.25` seconds.
- _`eyesOffScreenAllowedTime`_: Maximum time (in seconds) of _eyes tracking lost_ before going back to _looking for face_. Default is `0.25` seconds.
- _`numberOfFacesAllowed`_: Maximum number of faces allowed. If more faces are detected, the tracking is set to interrupt. Default is `1`.
- _`authenticationMinimumInterval`_: Minimum frequency (in seconds) of authentication. Default is `2` seconds.
- _`lowLightThreshold`_: Minimum allowed of estimated ambient luminosity. If the estimators return a _Lux value_ minor than the threshold, the delegate's `onLightWarning` will be called. Tweak the value of this parameter so that the alert gets called whenever your app expects it. Default is `900`.

You can save your _`WCPUAConfig`_ to _`UserDefaults`_ by calling:

```swift
let PUAOneParameters = WCPUAConfig.WCPUAOneConfig()
// ... Edit PUAOne parameters

let PUATwoParameters = WCPUAConfig.WCPUATwoConfig()
// .. Edit PUATwo parameters

let parameters = WCPUAConfig(PUAOneConfig: PUAOneParameters, PUATwoConfig: PUATwoParameters)
parameters.persistConfig()
```

You can restore your saved config by calling:

```swift
let parameters = WCPUAConfig(PUAOneConfig: WCPUAConfig.WCPUAOneConfig(), PUATwoConfig: WCPUAConfig.WCPUATwoConfig())
parameters.restoreConfig()
```

## Advanced - Detailed delegates

The _`WCPUATwo`_ has a lot more features than the _`WCPUAOne`_. The events detected by those features are simplified so that _`WCPUAOne`_ and _`WCPUATwo`_ can share the same delegate API. However, there are some scenarios where catching the _`WCPUATwo`_ events without simplification is needed. That is why, if you are certain that your _`WCPUA`_ instance is running _`WCPUATwo`_, you have the option to confirm the _`WCPUATwoDetailedDelegate`_:

```swift
// MARK: - WCPUATwoDetailedDelegate
extension ViewController: WCPUATwoDetailedDelegate {
    func onMultipleFacesDetected(numberOfFaces faces: Int) {

    }

    func onEyesOutOfScreen() {

    }

    func onFaceOutOfScreen() {

    }

    func onFaceDetected() {

    }

    func onFrameCaptured(frame: CVPixelBuffer) {

    }
}
```

Then you can set your class as a detailed delegate of the _`WCPUATwo`_ manager:

```swift
if let PUATwoManager = self.PUA?.manager as? WCPUATwo {
    PUATwoManager.detailedDelegate = self
}
```

Usually, you would do this before calling _`self.PUA?.config()`_

## Advanced - Getting camera frames

Sometimes, having access to the camera frames used by the PUA could be very useful. The way you can achieve differs from _`WCPUAOne`_ to _`WCPUATwo`_.

### Getting video preview layer from WCPUAOne

The _`WCPUAOne`_ can provide the video preview layer that is being used by the PUA, for example:

```swift
if let manager = self.PUA?.manager as? WCPUAOne {
    let videoPreview = manager.getVideoPlayer()
}
```

_`videoPreview`_ is an instance of _`AVCaptureVideoPreviewLayer`_. You can use _`videoPreview`_ to display the video.

### Getting camera frames from WCPUATwo

Each time a new frame is processed by the _`WCPUATwo`_ manager, the delegate's _`onFrameCaptured`_ method gets called. You can then use the provided _`CVPixelBuffer`_ as you need.
