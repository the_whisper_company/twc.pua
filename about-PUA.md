
# Persistent User Authentication Functionality

Figure 1.  The PUA SDK represents the HMI (Human-Machine Interface) between the application and the user.  Internally the app may use the device biometric sensors to authenticate the user or any other API for access management.  After that, PUA continues to use biometrics–beyond access–to validated that the (just) authenticated user is the one continuing using the app or device.  When it is not or a second (or more) face appears (visual hacker), then the app is notified of the violation and restrict access of the user to the app/device.  For example, it will pause the season and obfuscates the screen.

Your app may use any of the available APIs for IAM (Identity & Access Management), PAM (Privilege Access Management), MFA (Multi-Factor Authentication), and/or IDaaS (identity as a Service) to determine the user’s identity for initial access.  After that, your app can rely on PUA to guarantee that that the (just-authenticated) user remains operating the app, beyond just “access”.  We compliment your existing sign-on authentication.  Ensuring continuous “use” is only done by the authenticated user.  PUA can also work with Secure SSO (Single Sign-On) to Thick Client, Federated and Web apps, as well as Role-based, Behavior-based, or IoT-based access controls.  You can continue to use your existing password management, identity administration and governance , and device and enterprise mobility management tools.

PUA returns to your app OnAuthXX signal that is onAuthSuccess when the authentication was successful and onAuthFailed when it failed.  The developer will have delegate functions for each instance.

## Initial Authentication

Initially, after configuration (Config in Fig. 2), PUA enters the SearchFace mode: it “searches” (waits) for a face using the front (or desktop) camera.  Once a face has been detected, it uses Authenticate to first authenticate the user (AuthUser).  The preferred method is to use biometrics (Face ID or Fingerprint) for their accuracy, but the developer may use other form(s) of authentication, namely, passwords, tokens, or another User Authentication API service.  The API will respond with onAuthSuccess event that delegates to the main app for the developer to perform any desired operation to handle this event.

Figure 2.  The PUA SDK when initialized, it creates a thread in the processor.  Internally the app may use the device biometric sensors to authenticate the user or any other API for access management and get configured with the app’s selected parameters.  After that, waits for the main app to issue the start command.  PUA enters in a loop that searches for a face, SearchFace.  When the face is detected, it asks the app to acknowledge that the face in the camera field of view (CFOV) is authorized to use the app/device.   When authorized, PUA enters in the TrackFace mode, where the camera tracks the just authorized face and as long as the face remains in the CFOV and no other face is detected, it continues to let the main app know by issuing a OnAuthSuccess.  If the user’s face leaves the CFOV for a specified amount of time (determined in the Config by the main app parameters) or more than one face (visual hacker) is detected, the PUA issues a OnAuthFailure.   As you can see in the SearchFace and TrackFace flow diagrams, PUA captures an image frame from the frontal (user side) camera, checks that there is only one face, OneFace.  Then, checks that the user is engaged (user’s attention, baby tracking the user’s gaze), GazeOn.  The first time a new face is detected (in SearchFace), then it validates that the user is authorized (Owner) and if it is, the issues the OnAuthSuccess.  For consecutive cases (TrackFace), PUA issues the OnAuthSuccess.  If any of the tree or two checks (OneFace?, GazeOn?, or Owner?) is false, PUA issues a OnAuthFailure.   

## Continuous Authorization

After the user has been properly authenticated, PUA’s API switches (to save battery power) to TrackFace mode: Face-Tracking using the mobile (or desktop) frontal camera, to guarantee that the “just-authenticated” user remains in the camera view and the gaze (direction of the AuthUser eyes) is focused on the screen.

There are three conditions that will create an exception: onAuthFailed.  These are:

If the AuthUser’s face is not in the camera field-of-view (CFOV) for a (developer-specified) amount of time

If the AuthUser’s gaze is not looking at the screen (Attention) for a (developer-specified) amount of time,

If a second (or multiple) face(s) appear in the CDOV (NotAlone).

These are “continuously” checked.  The timeout–times are determined by the developer or the developer may provide a configuration menu for the AuthUser to select.  Figure 2 shows the provided UX for the latter.Figure 3. PUA flow diagram.  The left side shows the different functions of the PUA SDK.  To the right are the different delegate functions that the main app developer must implement in their code.  These will be called by PUA when any of the triggering events occur:  onConfigFails onGenericError, onLightWarning, onAuthSuccess, or onAuthFailed.

When the onAuthFailed is called (delegated to the main app), the developer will insert the methods to handle these events, namely, what form of Resolution they desire to execute.  For example: 

Obfuscate the screen. (This is what we implemented in our demonstrator app invisible-ink™). We recommend displaying the company logo or a branding video.

Lock the phone.

Sound an alarm.

Display a warning with exception’s reason (gaze-timed-out, faced-not-visible-timed-out, second-face, wrong-face, etc.)

Perform a haptic operation (e.g., vibration pattern, flashing pattern, etc.)

Send an alert (message, email, SMS, etc.) to a designated party.

Take a picture of the user with the frontal camera; and/or video (videos with both cameras); and/or record sound; and/or send with an alert (message, email, SMS, etc.) to a designated party (IT manager, security firm, phone owner, message owner, authorities, etc.)

Push the App to the background.

Close the App.

## Re-Authentication

After onAuthFailed is issued (regardless of the reason), PUA will return to the SearchFace mode, unless the developer decides to exit the application.  So if the user “Halts” (“Pauses”) the execution and awaits for the user to “come back” to re-engage with the App, it will “Resume” operations where it left-off on the onAuthSuccess event.

## Low Lighting

An additional requirement for face tracking is that it needs light.  If the combination of ambient light and the screen “glare” is below a given threshold, it will trigger the onLightWarning event.  Here, the developer may switch modes to a lighter background or increase the screen intensity.  This is needed for face tracking, not for Face ID.

## PUA Settings

Figure 2 shows the embedded PUA settings GUI, that is available to the developer to allow (if so desired) the AuthUser to set the timeouts for face-not-present (Refresh rate), Eyes-off-screen, and Figure 4. PUA setting dialog.  This image shows an embedded function of PUA that gives the user controls over the PUA settings.  The main app developer may use it or pre-set them and not allow the end user to change them.
Number of faces allowed.  There is also a setting to trigger an event if the ambient light is below a given threshold (Low-light threshold), that triggers the onLightWarning event–as previously explained.

Figure 2. PUA preferences dialog.  This dialog is available for the developers to use.

## Contact Information

For additional information, request an API for testing, or Licensing terms, please contact: Alejandro Fernandez, CEO at alejandro@TheWhisperCompany.com or +1 (512) 963-4498. 
