import UIKit

class MainNavigationViewController: UINavigationController {

    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
}

// MARK: - UINavigationControllerDelegate
extension MainNavigationViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool){
        
        // if showing the chats page
        if let dest = viewController as? MainViewController {
            dest.nav = self
        }
    }
}

