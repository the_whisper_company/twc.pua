import UIKit
import AVFoundation
import VideoToolbox
import PUA

// MARK: - Definition
class DemoViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var PUAStatusLabel: UILabel!
    @IBOutlet weak var PUAMessageLabel: UILabel!
    @IBOutlet weak var bufferImageView: UIImageView!
    @IBOutlet weak var securedText: UILabel!
    
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    public var nav: UINavigationController?
    
    private var PUA: WCPUA?
    private var PUAlowLightWarned = false
    public var state = "hidden"

    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settingsItem = UIBarButtonItem(image: UIImage(systemName: "gear") , style: .plain, target: self, action: #selector(self.goToSettings))
        settingsItem.title = "Settings"
        navigationItem.rightBarButtonItem = settingsItem
    }
    
    // MARK: - Go to settings
    @objc func goToSettings(){
        if WCPUA.getVersion() == "2.0" {
            self.performSegue(withIdentifier: "GoToPUATwoSettings", sender: nil)
        }
        else {
            self.performSegue(withIdentifier: "GoToPUAOneSettings", sender: nil)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "Searching face to request authentication"
        
        if WCPUA.getVersion() == "1.0" {
            self.setupPUA {
                guard let PUA1 = self.PUA?.manager as? WCPUAOne else {
                    DispatchQueue.main.async {
                        print("Unable to access front camera for PUA demo")
                        self.PUAStatusLabel.text = "Demo not available"
                        self.PUAMessageLabel.text = "Unable to access front camera for PUA demo"
                    }
                    return
                }
                
                self.videoPreviewLayer = PUA1.getVideoPlayer()
                DispatchQueue.main.async {
                    self.setupLivePreview()
                }
            }
        }
        else {
            self.setupPUA()
        }
    }
    
    // MARK: - PUA setup
    func setupPUA(_ callback: (() -> Void)? = nil){
        self.PUAlowLightWarned = false
        self.PUA = WCPUA(parameters: DemoConfig.instance.PUAConfig)
        self.PUA?.config()
        DispatchQueue.global(qos: .userInitiated).async {
            self.PUA?.start()
            callback?()
        }
        self.PUA?.delegate = self
        if WCPUA.getVersion() == "2.0" {
            let PUA2 = self.PUA!.manager as! WCPUATwo
            PUA2.detailedDelegate = self
        }
        
    }
    
    // MARK: - Live preview setup
    func setupLivePreview() {
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        self.videoPreviewLayer.frame = self.previewView.bounds
    }
    
    // MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.PUA?.stop()
    }
    
    // MARK: - Animate show secure text
    func animateShowSecureText() {
        UIView.animate(withDuration: 0.5) {
            self.securedText.alpha = 1.0
        }
    }
    
    // MARK: - Animate hide secure text
    func animateHideShowSecureText() {
        UIView.animate(withDuration: 0.5) {
            self.securedText.alpha = 0.0
        }
    }
}

// MARK: - PUA delegate
extension DemoViewController: WCPUADelegate {
    func onConfigFailed() {
        PUAStatusLabel.text = "Demo not available"
        PUAMessageLabel.text = "Failed to configurate PUA"
        backgroundView.backgroundColor = UIColor.systemRed
    }
    
    func onAuthFailed() {
        if WCPUA.getVersion() == "2.0" { return }
        
        if self.state == "visible" {
            self.animateHideShowSecureText()
        }
        
        self.state = "hidden"
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "Authentication failed"
        backgroundView.backgroundColor = UIColor.systemRed
    }
    
    func onAuthSuccess() {
        if self.state == "hidden" {
            self.animateShowSecureText()
        }
        self.state = "visible"
        PUAStatusLabel.text = "PUA Unlocked"
        PUAMessageLabel.text = "Biometric authentication sucessfull"
        backgroundView.backgroundColor = UIColor.systemBlue
    }
    
    func onGenericError() {
        PUAStatusLabel.text = "Demo not available"
        PUAMessageLabel.text = "PUA fatal generic error"
        backgroundView.backgroundColor = UIColor.systemRed
    }
    
    func onLightWarning() {
        guard !self.PUAlowLightWarned else {
            return
        }
        
        self.PUAlowLightWarned = true
        let alert = UIAlertController(title: "Low lightning",
                                      message: "PUA might not work properly.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - WCPUATwo detailed delegate
extension DemoViewController: WCPUATwoDetailedDelegate {
    func onFrameCaptured(frame: CVPixelBuffer) {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(frame, options: nil, imageOut: &cgImage)
        
        guard let img = cgImage else {
            print("failed to get frame UIImage")
            return
        }
        
        let image = UIImage(cgImage: img)
        guard let rotatedImage = image.rotate(radians: .pi / 2) else {
            print("failed to rotate image")
            return
        }
        
        self.bufferImageView.image = rotatedImage
    }
    
    func onMultipleFacesDetected(numberOfFaces faces: Int) {
        if self.state == "visible" {
            self.animateHideShowSecureText()
        }
        
        self.state = "hidden"
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "Maximum number of allowed faces"
        backgroundView.backgroundColor = UIColor.systemRed
    }
    
    func onEyesOutOfScreen() {
        if self.state == "visible" {
            self.animateHideShowSecureText()
        }
        
        self.state = "hidden"
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "You are not looking at the screen"
        backgroundView.backgroundColor = UIColor.systemRed
    }
    
    func onFaceOutOfScreen() {
        if self.state == "visible" {
            self.animateHideShowSecureText()
        }
        
        self.state = "hidden"
        PUAStatusLabel.text = "PUA Locked"
        PUAMessageLabel.text = "Your face is outside the screen"
        backgroundView.backgroundColor = UIColor.systemRed
    }
    
    func onFaceDetected() {
        PUAStatusLabel.text = "PUA authenticating"
        PUAMessageLabel.text = "Face detected, authenticating"
        backgroundView.backgroundColor = UIColor.systemRed
    }
}

// MARK: - WCPUALoggerProtocol
extension DemoViewController: WCPUALoggerProtocol {
    func error(_ object: Any) {
        print(object)
    }

    func info(_ object: Any) {
        print(object)
    }

    func debug(_ object: Any) {
        print(object)
    }

    func verb(_ object: Any) {
        print(object)
    }

    func warn(_ object: Any) {
        print(object)
    }

    func severe(_ object: Any) {
        print(object)
    }
}
