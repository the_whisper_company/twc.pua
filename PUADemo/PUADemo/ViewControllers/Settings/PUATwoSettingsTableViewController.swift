
import UIKit
import PUA

class PUATwoSettingsViewController: UITableViewController {

    @IBOutlet weak var PUAVersionLabel: UILabel!
    
    @IBOutlet weak var PUATwoFaceOffScreenCurrentValueLabel: UILabel!
    @IBOutlet weak var PUATwoFaceOffScreenSlider: UISlider!
    
    @IBOutlet weak var PUATwoEyesOffScreenCurrentValueLabel: UILabel!
    @IBOutlet weak var PUATwoEyesOffScreenSlider: UISlider!
    
    @IBOutlet weak var PUATwoNumberOfFacesCurrentValueLabel: UILabel!
    
    @IBOutlet weak var PUATwoLowLightThresholdCurrentValueLabel: UILabel!
    @IBOutlet weak var PUATwoLowLightThresholdSlider: UISlider!
    
    private var stepsLabels = ["Instant","Fast","Medium","Light"]
    private var stepsValues = [0.25,1,4,10]
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        self.PUAVersionLabel.text = "PUA V" + WCPUA.getVersion()
        
        PUATwoFaceOffScreenSlider.value = Float(self.stepsValues.firstIndex( of: DemoConfig.instance.PUAConfig.PUATwo.faceOffScreenAllowedTime)!)
        PUATwoEyesOffScreenSlider.value = Float(DemoConfig.instance.PUAConfig.PUATwo.eyesOffScreenAllowedTime)
        
        self.PUATwoFaceOffScreenCurrentValueLabel.text = "value: " + self.stepsLabels[Int(PUATwoFaceOffScreenSlider.value)]
        self.PUATwoEyesOffScreenCurrentValueLabel.text = "value: \(Int(PUATwoEyesOffScreenSlider.value))"
        
        self.PUATwoNumberOfFacesCurrentValueLabel.text = "\(DemoConfig.instance.PUAConfig.PUATwo.numberOfFacesAllowed)"
        
        PUATwoLowLightThresholdSlider.value = Float(DemoConfig.instance.PUAConfig.PUATwo.lowLightThreshold)
        self.PUATwoLowLightThresholdCurrentValueLabel.text = "value: \(Int(PUATwoLowLightThresholdSlider.value))"
    }
    
    // MARK: Actions
    
    @IBAction func PUATwoFaceOffScreenSliderChanged(_ sender: Any) {
        self.PUATwoFaceOffScreenSlider.value = round(self.PUATwoFaceOffScreenSlider.value)
        self.PUATwoFaceOffScreenCurrentValueLabel.text = "value: " + self.stepsLabels[Int(PUATwoFaceOffScreenSlider.value)]
        DemoConfig.instance.PUAConfig.PUATwo.faceOffScreenAllowedTime = Double(self.stepsValues[Int(self.PUATwoFaceOffScreenSlider.value)])
    }
    
    @IBAction func PUATwoEyesOffScreenSliderChanged(_ sender: Any) {
        self.PUATwoEyesOffScreenSlider.value = round(self.PUATwoEyesOffScreenSlider.value)
        self.PUATwoEyesOffScreenCurrentValueLabel.text = "value: \(Int(PUATwoEyesOffScreenSlider.value))"
        DemoConfig.instance.PUAConfig.PUATwo.eyesOffScreenAllowedTime = Double(PUATwoEyesOffScreenSlider.value)
    }
    
    @IBAction func PUATwoLowLightThresholdSliderChanged(_ sender: Any) {
        self.PUATwoLowLightThresholdSlider.value = round(PUATwoLowLightThresholdSlider.value)
        self.PUATwoLowLightThresholdCurrentValueLabel.text = "value: \(Int(PUATwoLowLightThresholdSlider.value))"
        DemoConfig.instance.PUAConfig.PUATwo.lowLightThreshold = Double(PUATwoLowLightThresholdSlider.value)
    }
    
    // MARK: - Styling separators
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView? = UIView()
        headerView?.backgroundColor = UIColor(named: "WCDarkGray")
        
        let sectionLabel = UILabel(frame: CGRect(x: 12, y: 4, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        sectionLabel.font = UIFont(name: "Exo-Regular", size: 16)
        sectionLabel.textColor = UIColor(named: "WCYellow")
        switch section {
        case 0:
            sectionLabel.text = "Parameters"
            break
        case 1:
            sectionLabel.text = "Active version"
            break
        default:
            sectionLabel.text = ""
            break
        }
        sectionLabel.sizeToFit()
        headerView?.addSubview(sectionLabel)
        
        return headerView
    }
    
    // MARK: - Row selection
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 0 && indexPath.row == 3 {
            // show timer picker
            let pickerAlert = PickerAlert(choices: DemoConfig.instance.PUAConfig.PUATwo.validNumberOfFaces, title: "Select number of faces")
            let alert = pickerAlert.getAlert()
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                guard let selected = pickerAlert.selected else {
                    return
                }
                self.PUATwoNumberOfFacesCurrentValueLabel.text = selected
                DemoConfig.instance.PUAConfig.PUATwo.numberOfFacesAllowed = Int(selected)!
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    @IBAction func infoClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Refresh rate info",
                                      message: "A faster value will be more secure but consumes more battery.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
