
import UIKit
import PUA

// MARK: - Definition
class PUAOneSettingsViewController: UITableViewController {
    
    @IBOutlet weak var PUAVersionLabel: UILabel!
    
    @IBOutlet weak var PUAOneAuthenticationFrecuenceCurrentValueLabel: UILabel!
    @IBOutlet weak var PUAOneAuthenticationFrecuenceSlider: UISlider!
    
    @IBOutlet weak var PUAOneLowLightThresholdCurrentValueLabel: UILabel!
    @IBOutlet weak var PUAOneLowLightThresholdSlider: UISlider!

    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.allowsSelection = false
        
        self.PUAVersionLabel.text = "PUA V" + WCPUA.getVersion()
        
        PUAOneAuthenticationFrecuenceSlider.value = Float(DemoConfig.instance.PUAConfig.PUAOne.interval)
        self.PUAOneAuthenticationFrecuenceCurrentValueLabel.text = "value: \(Int(PUAOneAuthenticationFrecuenceSlider.value))"
        
        PUAOneLowLightThresholdSlider.value = Float(DemoConfig.instance.PUAConfig.PUAOne.lowLightThreshold)
        self.PUAOneLowLightThresholdCurrentValueLabel.text = "value: \(Int(PUAOneLowLightThresholdSlider.value))"
    }
    
    // MARK: Actions
    
    @IBAction func PUAOneAuthenticationFrecuenceSliderChanged(_ sender: Any) {
        self.PUAOneAuthenticationFrecuenceSlider.value = round(PUAOneAuthenticationFrecuenceSlider.value)
        self.PUAOneAuthenticationFrecuenceCurrentValueLabel.text = "value: \(Int(PUAOneAuthenticationFrecuenceSlider.value))"
        DemoConfig.instance.PUAConfig.PUAOne.interval = Int(PUAOneAuthenticationFrecuenceSlider.value)
    }
    
    @IBAction func PUAOneLowLightThresholdSliderChanged(_ sender: Any) {
        self.PUAOneLowLightThresholdSlider.value = round(PUAOneLowLightThresholdSlider.value)
        self.PUAOneLowLightThresholdCurrentValueLabel.text = "value: \(Int(PUAOneLowLightThresholdSlider.value))"
        DemoConfig.instance.PUAConfig.PUAOne.lowLightThreshold = Double(PUAOneLowLightThresholdSlider.value)
    }
    
    // MARK: - Styling separators
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView? = UIView()
        headerView?.backgroundColor = UIColor.systemBackground
        
        let sectionLabel = UILabel(frame: CGRect(x: 12, y: 4, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        sectionLabel.textColor = UIColor.systemBlue
        switch section {
        case 0:
            sectionLabel.text = "Parameters"
            break
        case 1:
            sectionLabel.text = "Current active version"
            break
        default:
            sectionLabel.text = ""
            break
        }
        sectionLabel.sizeToFit()
        headerView?.addSubview(sectionLabel)
        
        return headerView
    }

}
