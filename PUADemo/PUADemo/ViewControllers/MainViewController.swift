import UIKit
import PUA

class MainViewController: UIViewController {
    
    public var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDemo" {
            let dest = segue.destination as! DemoViewController
            dest.nav = self.nav
        }
    }
    
    @IBAction func demoClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "GoToDemo", sender: nil)
    }
    
    @IBAction func settingsDemoClicked(_ sender: Any) {
        if WCPUA.getVersion() == "2.0" {
            self.performSegue(withIdentifier: "GoToPUATwoSettings", sender: nil)
        }
        else {
            self.performSegue(withIdentifier: "GoToPUAOneSettings", sender: nil)
        }
    }
}
