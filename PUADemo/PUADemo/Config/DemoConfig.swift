
import UIKit
import PUA

// MARK: - Singleton definition
class DemoConfig {
    static let instance = DemoConfig()
    let PUAConfig: WCPUAConfig
    
    private init(){
        let PUAOneParameters = WCPUAConfig.WCPUAOneConfig()
        PUAOneParameters.lowLightThreshold = 600.0
        
        let PUATwoParameters = WCPUAConfig.WCPUATwoConfig()
        PUATwoParameters.lowLightThreshold = 400.0
        
        self.PUAConfig = WCPUAConfig(PUAOneConfig: PUAOneParameters, PUATwoConfig: PUATwoParameters)
    }
}
