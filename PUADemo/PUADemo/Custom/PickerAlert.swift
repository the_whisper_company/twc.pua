
import UIKit

// MARK: - Definition
class PickerAlert: NSObject {
    
    var choices: [String] = []
    var pickerView = UIPickerView()
    var selected: String? = nil
    var title: String = "PickerAlert"
    
    // MARK: - Init
    init(choices: [String], title: String) {
        self.choices = choices
        self.title = title
        self.selected = choices.isEmpty ? nil : choices[0]
    }
    
    // MARK: - Get alert
    func getAlert() -> UIAlertController {
        let alert = UIAlertController(title: self.title, message: "\n\n\n\n\n\n\n", preferredStyle: .alert)
        
        self.pickerView = UIPickerView(frame: CGRect(x: 5, y: 20, width: 250, height: 140))
        
        alert.view.tintColor = UIColor(named: "WCAlertsTint")
        alert.view.addSubview(pickerView)
        pickerView.dataSource = self
        pickerView.delegate = self
        
        return alert
    }
}

// MARK: - UIPickerViewDelegate
extension PickerAlert: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selected = self.choices[row]
    }
}

// MARK: - UIPickerViewDataSource
extension PickerAlert: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.choices.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.choices[row]
    }
}
